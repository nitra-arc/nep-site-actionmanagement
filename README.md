# ActionManagementBundle

## Описание
Данный бандл предназначен для работы (вывода, обработки) с:

* управлением акциями (ActionManagement) - вся информация об акциии
* акциями по товару (ActionProduct), сумма или процент скидки по товару
## ActionManagementController
* actionProductAction - функция передает в шаблон объект акции по заданному товару
* listActions - выводит акции и время действия акции
* getActionProducts - выборка товаров по заданной акции

## Подключение
Для подключения данного модуля в проект необходимо:

* composer.json:

```json
{
    ...   
    "require": {
        ...
        "e-commerce-site/actionmanagementbundle": "1.6",
        ...
    }
    ...
}
```

* app/AppKernel.php:

```php
<?php

    //...
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\ActionManagementBundle\NitraActionManagementBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* app/config/routing.yml:

```yaml
#...
nitra_action_management:
    resource: "@NitraActionManagementBundle/Controller"
    type: annotation
#...
```