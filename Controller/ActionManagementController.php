<?php

namespace Nitra\ActionManagementBundle\Controller;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Nitra\StoreBundle\Helper\nlActions;
use Nitra\StoreBundle\Lib\Globals;

class ActionManagementController extends NitraController
{
    /**
     * Page of actions or one action
     *
     * @Route("/actions-page/{slug}", name="actions_page", defaults={"slug"=""})
     * @Template("NitraActionManagementBundle::actionPage.html.twig")
     *
     * @param string $slug
     * @return string Template context
     */
    public function actionsPageAction($slug)
    {
        if (!$slug) {
            return $this->listActions();
        }

        $slug   = mb_strtolower($slug, 'UTF-8');
        $action = $this->getActualQb()->field('aliasEn')->equals($slug)
            ->getQuery()->getSingleResult();

        if (!$action) {
            throw $this->createNotFoundException(sprintf('Action by alias "%s" not found', $slug));
        }

        $products = $this->paginate($this->getActionProducts($action), 'actionPage');

        $timeRemaining = false;
        if ($action->getDateTo()) {
            $timeRemaining = date_diff($action->getDateTo(), new \DateTime());
        }

        return array(
            'action'        => $action,
            'products'      => $products,
            'timeRemaining' => $timeRemaining,
        );
    }

    /**
     * Active actions module
     *
     * @Template("NitraActionManagementBundle::actionCategoriesModule.html.twig")
     *
     * @param int $limit
     *
     * @return array|Response Template context or empty response if actions not exists
     */
    public function actionsModuleAction($limit = 3)
    {
        $actions = $this->getActualQb()
            ->limit($limit)
            ->getQuery()->execute();

        if (!$actions->count()) {
            return new Response();
        }
        return array(
            'actions'   => $actions,
        );
    }

    /**
     * акционный блок на странице товара
     *
     * @Template("NitraActionManagementBundle::actionProductLabel.html.twig")
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     *
     * @return array|Response   Template context if action founded and empty response if no
     */
    public function actionProductAction($product)
    {
        // find action for product
        $action = nlActions::getProductAction($product);

        // if action not found - return empty response
        if (!$action) {
            return new Response();
        }

        // return template parameters
        return array(
            'thisProductAction' => $action,
        );
    }

    /**
     * Find products for actions
     *
     * @param \Nitra\ActionManagementBundle\Document\Action $action
     *
     * @return array|\Doctrine\MongoDB\Query\Builder
     */
    protected function getActionProducts($action)
    {
        $qb = $this->getDocumentManager()->getRepository('NitraProductBundle:Product')
            ->getDefaultQb()
            ->field('isActive')->equals(true);

        if ($action->getProducts()->count()) {
            foreach ($action->getProducts() as $product) {
                $qb->addOr($qb->expr()->field('_id')->equals($product->getProductId()));
            }
        } elseif ($action->getCategory() || $action->getBrand()) {
            $modelsQb = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Model')
                ->distinct('_id');

            if ($action->getBrand()) {
                $modelsQb->field('brand.id')->equals($action->getBrand()->getId());
            }
            if ($action->getCategory()) {
                $modelsQb->field('category.id')->equals($action->getCategory()->getId());
            }

            $modelsIds = $modelsQb->getQuery()->execute()->toArray();

            $qb->field('model.$id')->in($modelsIds);
        } else {
            return array();
        }

        return $qb;
    }

    /**
     * List of actual actions
     *
     * @return Response
     */
    protected function listActions()
    {
        $actions = $this->getActualQb()
            ->field('dateTo')->exists(true)
            ->getQuery()->execute();

        $timeRemaining = array();

        foreach ($actions as $key => $value) {
            $date = new \DateTime();
            $diff = date_diff($value->getDateTo(), $date);
            $timeRemaining[$key] = $diff;
        }

        return $this->render('NitraActionManagementBundle::actionsList.html.twig', array(
            'actions'       => $actions,
            'timeRemaining' => $timeRemaining,
        ));
    }
    
    /**
     * Get query builder for actual actions
     *
     * @return \Doctrine\MongoDB\Query\Builder
     */
    protected function getActualQb()
    {
        $qb = $this->getDocumentManager()->createQueryBuilder('NitraActionManagementBundle:Action');
        $qb->addOr($qb->expr()
            ->field('dateFrom')->lt(date_create("now"))
            ->field('dateTo')->gt(date_create("now"))
        )->addOr($qb->expr()
            ->field('dateFrom')->exists(false)
            ->field('dateTo')->gt(date_create("now"))
        )->addOr($qb->expr()
            ->field('dateFrom')->lt(date_create("now"))
            ->field('dateTo')->exists(false)
        )->addOr($qb->expr()
            ->field('dateFrom')->exists(false)
            ->field('dateTo')->exists(false)
        )
        ->sort('priority', 'asc');

        return $qb;
    }
}