<?php

namespace Nitra\ActionManagementBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class ActionProduct
{    
    /**
     * @ODM\Id(strategy="AUTO")
     */
    private $id;

    /**
     * Товар
     * @ODM\String
     */
    private $productId;
    
    /**
     * Тип скидки (процент или сумма)
     * @ODM\String
     */
    private $discountType;
    
    /**
     * Сумма или процент скидки
     * @ODM\Float
     */
    private $discount;
    
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get productId
     * @return string
     */
    public function getProductId()
    {
        return $this->productId;
    }
    
    /**
     * Set productId
     * @param string $productId
     * @return self
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
        return $this;
    }
    
    /**
     * Get discountType
     * @return string
     */
    public function getDiscountType()
    {
        return $this->discountType;
    }
    
    /**
     * Set discountType
     * @param string $discountType
     * @return self
     */
    public function setDiscountType($discountType)
    {
        $this->discountType = $discountType;
        return $this;
    }
    
    /**
     * Get discount
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }
    
    /**
     * Set discount
     * @param float $discount
     * @return self
     */
    public function setDiscount($discount)
    {
        $this->discount= $discount;
        return $this;
    }
}