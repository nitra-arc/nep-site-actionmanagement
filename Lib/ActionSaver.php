<?php

namespace Nitra\ActionManagementBundle\Lib;

use Symfony\Component\DependencyInjection\Container;
use Doctrine\MongoDB\Query\Builder;

class ActionSaver
{
    public static function saveActionsToCache(Container $container, array $store)
    {
        $cache  = $container->get('cache_apc');
        $mdb    = $container->getParameter('mongo_database_name');
        $dm     = $container->get('doctrine_mongodb.odm.document_manager');
        $key    = $mdb . '_nitra_actions_by_product_ids_' . $store['host'];
        // если нет акций в кэше
        if (!$cache->contains($key)) {
            // получить все акции, которые являются актуальными 
            // отсортированные по приоритету (убывание), чтоб значения более приоретных перетирали менее приоритные
            $actions = self::getActualQb($dm)
                ->sort('priority', 'desc')
                ->getQuery()->execute();
            $Products = array();
            // цикл по полученным акциям
            foreach ($actions as $action) {
                $qb = self::getActionProducts($store, $dm, $action);
                $products = ($qb instanceof Builder) ? $qb->getQuery()->execute() : array();
                foreach ($products as $product) {
                    $Products[$product->getId()] = self::getProductDiscount($action, $product);
                }
            }
            // записываем в кэш на час
            $cache->save($key, $Products, 60 * 60);
        }

        self::updateBadgeSortsProductsActions($dm);
    }

    /**
     * @param array $store
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @param \Nitra\ActionManagementBundle\Document\Action $action
     * @return array|Builder
     */
    protected static function getActionProducts($store, $dm, $action)
    {
        // получение текущего магазина
        $qb = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->field('isActive')->equals(true)
            ->field('storePrice.' . $store['id'] . '.price')->exists(true);
        
        if ($action->getProducts()->count()) {
            foreach ($action->getProducts() as $product) {
                $qb->addOr($qb->expr()->field('_id')->equals($product->getProductId()));
            }
        } elseif ($action->getCategory() || $action->getBrand()) {
            $modelsQb = $dm->createQueryBuilder('NitraProductBundle:Model')
                ->distinct('_id');

            if ($action->getBrand()) {
                $modelsQb->field('brand.id')->equals($action->getBrand()->getId());
            }
            if ($action->getCategory()) {
                $modelsQb->field('category.id')->equals($action->getCategory()->getId());
            }

            $modelsIds = $modelsQb->getQuery()->execute()->toArray();

            $qb->field('model.$id')->in($modelsIds);
        } else {
            return array();
        }

        return $qb;
    }

    /**
     * @param \Nitra\ActionManagementBundle\Document\Action $action
     * @param \Nitra\ProductBundle\Document\Product $product
     * @return array
     */
    protected static function getProductDiscount($action, $product)
    {
        if ($action->getProducts()->count()) {
            foreach ($action->getProducts() as $prod) {
                if ($product->getId() == $prod->getProductId()) {
                    return array(
                        'id'        => $action->getId(),
                        'type'      => $prod->getDiscountType(),
                        'discount'  => $prod->getDiscount(),
                    );
                }
            }
        }
        
        return array(
            'id'        => $action->getId(),
            'type'      => 'percent',
            'discount'  => $action->getDiscount() ? $action->getDiscount() : 0,
        );
    }
    
    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     * @return Builder
     */
    protected static function getActualQb($dm)
    {
        $qb = $dm->createQueryBuilder('NitraActionManagementBundle:Action');
        $qb->addOr($qb->expr()
            ->field('dateFrom')->lt(date_create("now"))
            ->field('dateTo')->gt(date_create("now"))
        )->addOr($qb->expr()
            ->field('dateFrom')->exists(false)
            ->field('dateTo')->gt(date_create("now"))
        )->addOr($qb->expr()
            ->field('dateFrom')->lt(date_create("now"))
            ->field('dateTo')->exists(false)
        )->addOr($qb->expr()
            ->field('dateFrom')->exists(false)
            ->field('dateTo')->exists(false)
        );
        
        return $qb;
    }
    
    /**
     * Обновление бейджевого порядка сортировки товарам, для которых закончились акции
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     */
    protected static function updateBadgeSortsProductsActions($dm)
    {
        $actions = $dm->createQueryBuilder('NitraActionManagementBundle:Action')
            ->field('dateTo')->lt(date_create("now"))
            ->field('badgeUnset')->notEqual(true)
            ->sort('priority', 'desc')
            ->getQuery()->execute();

        foreach ($actions as $action) {
            $Products = self::getBadgeSortsQueryBuilder($dm, $action->getProducts(), $action->getCategory(), $action->getBrand())
                ->getQuery()->execute();
            foreach ($Products as $Product) {
                $productAction = self::getProductAction($dm, $Product, $action);
                $badgeSorts = array('all' => -1);
                if ($productAction) {
                    $badgeSorts[$productAction->getBadge()->getIdentifier()] = in_array('getSortOrder', get_class_methods($productAction->getBadge()))
                        ? $productAction->getBadge()->getSortOrder()
                        : 0;
                }
                $Product->setBadgeSorts($badgeSorts);
            }
            $action->SetBadgeUnset(true);
        }
        
        if ($actions->count()) {
            $dm->flush();
        }
    }

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager                         $dm
     * @param \Nitra\ProductBundle\Document\Product                         $product
     * @param \Nitra\ActionManagementBundle\Document\Action                 $Action
     * @return \Nitra\ActionManagementBundle\Document\Action|null
     */
    protected static function getProductAction($dm, $product, $Action)
    {
        $qb = $dm->createQueryBuilder('NitraActionManagementBundle:Action')
            ->field('_id')->notEqual($Action->getId())
            ->field('badge.$id')->exists(true);
        $qb->addAnd($qb->expr()
            ->addOr($qb->expr()
                ->field('dateFrom')->lt(date_create("now"))
                ->field('dateTo')->gt(date_create("now"))
            )->addOr($qb->expr()
                ->field('dateFrom')->exists(false)
                ->field('dateTo')->gt(date_create("now"))
            )->addOr($qb->expr()
                ->field('dateFrom')->lt(date_create("now"))
                ->field('dateTo')->exists(false)
            )->addOr($qb->expr()
                ->field('dateFrom')->exists(false)
                ->field('dateTo')->exists(false)
            )
        );
        $expr = $qb->expr();
        $expr->addOr($qb->expr()
            ->field('products.productId')->equals($product->getId())
        );

        $modelsQb = $dm->createQueryBuilder('NitraProductBundle:Model')
            ->distinct('_id');

        if ($product->getModel()->getBrand()) {
            $modelsQb->field('brand.id')->equals($product->getModel()->getBrand()->getId());
        }
        if ($product->getModel()->getCategory()) {
            $modelsQb->field('category.id')->equals($product->getModel()->getCategory()->getId());
        }

        $modelsIds = $modelsQb->getQuery()->execute()->toArray();

        $expr->field('model.$id')->in($modelsIds);
        $qb->addAnd($expr);

        return $qb
            ->sort('priority', 'asc')
            ->getQuery()->getSingleResult();
    }

    /**
     * @param \Doctrine\ODM\MongoDB\DocumentManager         $dm
     * @param \Doctrine\ODM\MongoDB\PersistentCollection    $products
     * @param \Nitra\ProductBundle\Document\Category|null   $category
     * @param \Nitra\ProductBundle\Document\Brand|null      $brand
     *
     * @return Builder
     */
    protected static function getBadgeSortsQueryBuilder($dm, $products, $category, $brand)
    {
        $qb     = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->field('badge.$id')->exists(false);

        if ($products && count($products)) {
            $ids = array();
            foreach ($products as $product) {
                $ids[] = new \MongoId($product->getProductId());
            }
            $qb->field('_id')->in($ids);
        } elseif ($category || $brand) {
            $modelsQb = $dm->createQueryBuilder('NitraProductBundle:Model')
                ->distinct('_id');

            if ($brand) {
                $modelsQb->field('brand.id')->equals($brand->getId());
            }
            if ($category) {
                $modelsQb->field('category.id')->equals($category->getId());
            }

            $modelsIds = $modelsQb->getQuery()->execute()->toArray();

            $qb->field('model.$id')->in($modelsIds);
        }

        return $qb;
    }
}